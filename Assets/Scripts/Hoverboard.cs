﻿using UnityEngine;

public class Hoverboard : MonoBehaviour
{
	private Smartpad.EventEmitter Smartpad;
	private Quaternion SmartpadOffset;
	private float RotationY;
	private float SpeedZ;
	private float MovementFactorAxis = 1000.0f;
	private float MovementFactorForwards = 0.5f;
	private float MovementFactorSideways = 0.7f;
	private float PositionLimitLeftRight;
	private float PositionLimitUp;
	private float RotationLimitLeftRight = 20.0f;
	private float RotationLimitUpDown = 20.0f;
	private bool Sideways;
	private bool SidewaysRightIsForwards;
	private Rigidbody rb;

	public void Init (Smartpad.EventEmitter smartpad, float speedZ, float positionLimitLeftRight, float positionLimitUp, bool sideways, bool sidewaysRightIsForwards)
	{
		Smartpad = smartpad;
		SpeedZ = speedZ;
		PositionLimitLeftRight = positionLimitLeftRight;
		PositionLimitUp = positionLimitUp;
		Sideways = sideways;
		Init (sidewaysRightIsForwards);
		rb = GetComponent<Rigidbody> ();

		SmartpadOffset = Quaternion.Euler (new Vector3 (0.0f, 90.0f, 90.0f));
	}

	public void Init (bool sidewaysRightIsForwards)
	{
		SidewaysRightIsForwards = sidewaysRightIsForwards;

		RotationY = Sideways ? (SidewaysRightIsForwards ? -90.0f : 90.0f) : 0.0f;
		Vector3 r = transform.localEulerAngles;
		r.y = RotationY;
		transform.localEulerAngles = r;

	}

	public void Freeze ()
	{
		rb.constraints = RigidbodyConstraints.FreezePosition;
	}

	public void Unfreeze ()
	{
		rb.constraints = RigidbodyConstraints.None;
	}

	void FixedUpdate ()
	{
		Vector3 p = LimitPosition (transform.localPosition);
		transform.localPosition = p;

		Vector3 r;
		if (Smartpad != null) {
			r = (Smartpad.MostRecent () * SmartpadOffset).eulerAngles;
		} else {
			float ih = Input.GetAxis ("Horizontal") * Time.deltaTime * MovementFactorAxis;
			float iv = Input.GetAxis ("Vertical") * Time.deltaTime * MovementFactorAxis;
			if (Sideways) {
				r.x = ih;
				r.z = iv;
				if (SidewaysRightIsForwards) {
					r.x = -r.x;
					r.z = -r.z;
				}
			} else {
				r.x = iv;
				r.z = -ih;
			}
		}
		r.y = RotationY;
		r = LimitRotation (r);
		transform.localEulerAngles = r;

		Vector3 v = rb.velocity;
		float x;
		float y;
		if (r.x > 180) {
			r.x -= 360;
		}
		if (r.z > 180) {
			r.z -= 360;
		}
		if (Sideways) {
			x = r.x * MovementFactorSideways;
			y = -r.z * MovementFactorSideways;
			if (SidewaysRightIsForwards) {
				x = -x;
				y = -y;
			}
		} else {
			x = -r.z * MovementFactorForwards;
			y = -r.x * MovementFactorForwards;
		}
		v.x = x;
		v.y = y;
		v.z = SpeedZ;
		rb.velocity = v;
	}

	private Vector3 LimitPosition (Vector3 p)
	{
		if (p.x > PositionLimitLeftRight) {
			p.x = PositionLimitLeftRight;
		}
		if (p.x < -PositionLimitLeftRight) {
			p.x = -PositionLimitLeftRight;
		}
		if (p.y > PositionLimitUp) {
			p.y = PositionLimitUp;
		}
		if (p.y < 0.0f) {
			p.y = 0.0f;
		}
		return p;
	}

	private Vector3 LimitRotation (Vector3 r)
	{
		if (r.x > RotationLimitUpDown && r.x < 180) {
			r.x = RotationLimitUpDown;
		}
		if (r.x < 360 - RotationLimitUpDown && r.x > 180) {
			r.x = 360 - RotationLimitUpDown;
		}
		if (r.z > RotationLimitLeftRight && r.z < 180) {
			r.z = RotationLimitLeftRight;
		}
		if (r.z < 360 - RotationLimitLeftRight && r.z > 180) {
			r.z = 360 - RotationLimitLeftRight;
		}
		return r;
	}
}
