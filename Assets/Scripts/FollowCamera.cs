﻿using UnityEngine;

public class FollowCamera : MonoBehaviour
{
	public bool ThirdPersonView {
		set {
			Offset = new Vector3 (0.0f, 1.7f, value ? -2.15f : 0.0f);
		}
	}

	public Transform Following;
	private Vector3 Offset;

	void Update ()
	{
		if (Following != null) {
			transform.position = Following.position + Offset;
		}
	}
}
