﻿using UnityEditor;
using UnityEngine;
using UnityEngine.UI;
using Smartpad;
using System;
using System.Collections.Generic;

public class Game : MonoBehaviour
{
	public enum State
	{
		SelectLevel,
		SelectMode,
		Calibrate,
		Run,
		Paused,
		Finished
	}

	public Dropdown SelectLevelDropdown;
	public Button SelectLevelContinueButton;
	public Dropdown SelectModeDropdown;
	public Toggle SelectModeSmartpadToggle;
	public InputField SelectModeSmartpadInputField;
	public Toggle SelectModeVRToggle;
	public Button SelectModeBackButton;
	public Button SelectModeContinueButton;
	public Dropdown CalibrateDropdown;
	public Button CalibrateBackButton;
	public Button CalibrateContinueButton;
	public Text RunCountdownText;
	public Text RunCountupText;
	public Button PausedStopButton;
	public Button PausedResumeButton;
	public InputField FinishedStatsInputField;
	public Button FinishedBackButton;
	public Button FinishedCopyButton;
	public FollowCamera NonVRCamera;
    public FollowCamera VRCamera;
	public AudioSource MusicPlayer;
	public AudioSource SoundPlayer;

	public GameObject HoverboardPrefab;
	public GameObject CollectablePrefab;
	public GameObject FloorPrefab;
	public GameObject SmartpadPrefab;

	public bool ThirdPersonView {
		get {
			return SelectModeDropdown.options [SelectModeDropdown.value].text.Substring (Sideways ? 4 : 3, 1) == "3";
		}
	}

	public bool Sideways {
		get {
			return SelectModeDropdown.options [SelectModeDropdown.value].text.Substring (0, 4) == "SIDE";
		}
	}

	public bool SidewaysRightIsForwards {
		get {
			return CalibrateDropdown.options [CalibrateDropdown.value].text.Substring (0, 5) == "Right";
		}
	}

	public bool SmartpadEnabled {
		get {
			return SelectModeSmartpadToggle.isOn;
		}
	}

	public bool VREnabled {
		get {
			return SelectModeVRToggle.isOn;
		}
	}

	private State CurrentState;

	public LevelData LevelData { get; private set; }

	private int Port;
	private bool[] Collected;
	private Hoverboard Hoverboard;
	private ICollection<GameObject> GeneratedObjects = new LinkedList<GameObject> ();
	private Smartpad.Smartpad Smartpad;
    private FollowCamera Camera;
    private bool CountdownRunning = false;
	private float CountdownDuration = 3.0f;
	private float CountdownFinishTime = 0.0f;

	public float CountupFinishTime { get; private set; }

	private float CountupElapsedTime = 0.0f;

	void Start ()
	{
		SelectLevelContinueButton.onClick.AddListener (StateSelectMode);

		SelectModeSmartpadToggle.onValueChanged.AddListener (delegate {
			SelectModeSmartpadInputField.interactable = SelectModeSmartpadToggle.isOn;
			UpdateUISelectModeContinueButton ();
		});
		SelectModeSmartpadInputField.onValueChanged.AddListener (delegate {
			Port = 0;
			int.TryParse (SelectModeSmartpadInputField.text, out Port);
			UpdateUISelectModeContinueButton ();
		});
		UpdateUISelectModeContinueButton ();
		SelectModeBackButton.onClick.AddListener (StateSelectLevel);
		SelectModeContinueButton.onClick.AddListener (StateCalibrate);

		CalibrateDropdown.onValueChanged.AddListener (delegate {
			Hoverboard.Init (SidewaysRightIsForwards);
		});
		CalibrateBackButton.onClick.AddListener (StateSelectMode);
		CalibrateContinueButton.onClick.AddListener (StateRun);

		PausedStopButton.onClick.AddListener (StateCalibrate);
		PausedResumeButton.onClick.AddListener (StateRun);

		FinishedBackButton.onClick.AddListener (StateCalibrate);
		FinishedCopyButton.onClick.AddListener (delegate {
			GUIUtility.systemCopyBuffer = FinishedStatsInputField.text;
		});

		Camera = NonVRCamera;
		VRCamera.gameObject.SetActive (false);

		StateSelectLevel ();
	}

	void Update ()
	{
		if (CurrentState == State.Calibrate) {
			if (Input.GetButtonDown ("CalibrateSmartpad")) {
				if (Smartpad != null) {
					CalibratedSmartpad calibratedSmartpad = Smartpad.GetComponent<CalibratedSmartpad> ();
					calibratedSmartpad.Calibrate ();
				}
			}
			if (Input.GetButtonDown ("RecenterVR")) {
				UnityEngine.VR.InputTracking.Recenter ();
			}
		} else if (CurrentState == State.Run) {
			if (CountdownRunning) {
				if (Time.time >= CountdownFinishTime) {
					StateRunCountdownFinished ();
				} else {
					RunCountdownText.text = Math.Ceiling (CountdownFinishTime - Time.time).ToString ();
				}
			} else {
				if (Input.GetButtonDown ("Cancel")) {
					StatePaused ();
				} else if (Time.time >= CountupFinishTime) {
					StateFinished ();
				} else {
					RunCountupText.text = Math.Ceiling (CountupFinishTime - Time.time).ToString ();
				}
			}
		}
	}

	private void Collect (Collectable collectable)
	{
		Collected [collectable.I] = true;
		SoundPlayer.Play ();
		Destroy (collectable.gameObject);
	}

	private void StateSelectLevel ()
	{
		CurrentState = State.SelectLevel;
		UpdateUI ();

		SelectLevelDropdown.options.Clear ();
		foreach (string assetPath in AssetDatabase.GetAssetPathsFromAssetBundle ("hoverboardlevels")) {
			Dropdown.OptionData optionData = new Dropdown.OptionData (assetPath.Substring (17));
			SelectLevelDropdown.options.Add (optionData);
		}
		SelectLevelDropdown.RefreshShownValue ();
	}

	private void StateSelectMode ()
	{
		CurrentState = State.SelectMode;
		UpdateUI ();

		UnloadLevel ();
		string assetPath = "Assets/Resources/" + SelectLevelDropdown.options [SelectLevelDropdown.value].text;
		if (!LoadLevelFile (assetPath)) {
			StateSelectLevel ();
		}
		if (!LoadMusic ()) {
			StateSelectLevel ();
		}
	}

	private void StateCalibrate ()
	{
		CurrentState = State.Calibrate;
		UpdateUI ();

		MusicPlayer.Stop ();
		UnloadLevel ();
		LoadLevel ();
		CountupElapsedTime = 0.0f;
	}

	private void StateRun ()
	{
		CurrentState = State.Run;
		UpdateUI ();

		CountdownFinishTime = Time.time + CountdownDuration;
		RunCountdownText.gameObject.SetActive (true);
		RunCountupText.text = Math.Ceiling (LevelData.duration - CountupElapsedTime).ToString ();
		CountdownRunning = true;
		Hoverboard.Freeze ();
	}

	private void StateRunCountdownFinished ()
	{
		CountdownRunning = false;
		MusicPlayer.Play ();
		CountupFinishTime = Time.time + LevelData.duration - CountupElapsedTime;
		RunCountdownText.gameObject.SetActive (false);
		Hoverboard.Unfreeze ();
	}

	private void StatePaused ()
	{
		CurrentState = State.Paused;
		UpdateUI ();

		MusicPlayer.Pause ();
		CountupElapsedTime = LevelData.duration + Time.time - CountupFinishTime;
		Hoverboard.Freeze ();
	}

	private void StateFinished ()
	{
		CurrentState = State.Finished;
		UpdateUI ();

		MusicPlayer.Stop ();
		Hoverboard.Freeze ();
		FinishedStatsInputField.text = "";
		foreach (bool collected in Collected) {
			FinishedStatsInputField.text += collected ? "1" : "0";
		}
	}

	private void UpdateUI ()
	{
		SelectLevelDropdown.gameObject.SetActive (CurrentState == State.SelectLevel);
		SelectLevelContinueButton.gameObject.SetActive (CurrentState == State.SelectLevel);
		SelectModeDropdown.gameObject.SetActive (CurrentState == State.SelectMode);
		SelectModeSmartpadToggle.gameObject.SetActive (CurrentState == State.SelectMode);
		SelectModeSmartpadInputField.gameObject.SetActive (CurrentState == State.SelectMode);
		SelectModeVRToggle.gameObject.SetActive (CurrentState == State.SelectMode);
		SelectModeBackButton.gameObject.SetActive (CurrentState == State.SelectMode);
		SelectModeContinueButton.gameObject.SetActive (CurrentState == State.SelectMode);
		CalibrateDropdown.gameObject.SetActive (CurrentState == State.Calibrate && Sideways);
		CalibrateBackButton.gameObject.SetActive (CurrentState == State.Calibrate);
		CalibrateContinueButton.gameObject.SetActive (CurrentState == State.Calibrate);
		RunCountdownText.gameObject.SetActive (CurrentState == State.Run);
		RunCountupText.gameObject.SetActive (CurrentState == State.Run);
		PausedStopButton.gameObject.SetActive (CurrentState == State.Paused);
		PausedResumeButton.gameObject.SetActive (CurrentState == State.Paused);
		FinishedStatsInputField.gameObject.SetActive (CurrentState == State.Finished);
		FinishedBackButton.gameObject.SetActive (CurrentState == State.Finished);
		FinishedCopyButton.gameObject.SetActive (CurrentState == State.Finished);
	}

	private void UpdateUISelectModeContinueButton ()
	{
		SelectModeContinueButton.interactable = !SmartpadEnabled || Port > 0 && Port < 65536;
	}

	private bool LoadLevelFile (string assetPath)
	{
		TextAsset level = (TextAsset)AssetDatabase.LoadAssetAtPath (assetPath, typeof(TextAsset));
		if (level == null) {
			return false;
		}
		LevelData = JsonUtility.FromJson<LevelData> (level.text);
		return true;
	}

	private bool LoadMusic ()
	{
		string[] musicGUIDs = AssetDatabase.FindAssets (LevelData.music);
		if (musicGUIDs.Length < 1) {
			return false;
		}
		string musicAssetPath = AssetDatabase.GUIDToAssetPath (musicGUIDs [0]);
		MusicPlayer.clip = (AudioClip)AssetDatabase.LoadAssetAtPath (musicAssetPath, typeof(AudioClip));
		return true;
	}

	private void LoadLevel ()
	{
		Smartpad.Smartpad smartpad = null;
		CalibratedSmartpad calibratedSmartpad = null;
		SlerpedSmartpad slerpedSmartpad = null;
		if (SmartpadEnabled) {
			smartpad = GameObject.Instantiate<GameObject> (SmartpadPrefab).GetComponent<Smartpad.Smartpad> ();
			smartpad.Port = Port;
			calibratedSmartpad = smartpad.GetComponent<CalibratedSmartpad> ();
			slerpedSmartpad = smartpad.GetComponent<SlerpedSmartpad> ();

			Transform t = new GameObject ("CalibratedSmartpadOffset").transform;
			t.localEulerAngles = new Vector3 (270.0f, 270.0f, 0.0f);
			calibratedSmartpad.Offset = t;

			Smartpad = smartpad;
		}

		Hoverboard = GameObject.Instantiate<GameObject> (HoverboardPrefab).GetComponent<Hoverboard> ();
		float speedZ = LevelData.length / LevelData.duration;
		Hoverboard.Init (slerpedSmartpad, speedZ, LevelData.width / 2.0f, LevelData.height, Sideways, SidewaysRightIsForwards);
		Hoverboard.Freeze ();

		NonVRCamera.gameObject.SetActive (!VREnabled);
		VRCamera.gameObject.SetActive (VREnabled);
		Camera = VREnabled ? VRCamera : NonVRCamera;
		Camera.Following = Hoverboard.transform;
		Camera.ThirdPersonView = ThirdPersonView;

		GameObject floor = GameObject.Instantiate<GameObject> (FloorPrefab);
		floor.transform.position = new Vector3 (0.0f, -1.0f, LevelData.length / 2.0f);
		floor.transform.localScale = new Vector3 (LevelData.width / 10.0f + 0.2f, 1.0f, LevelData.length / 10.0f + 0.2f);
		GeneratedObjects.Add (floor);

		Collected = new bool[LevelData.collectables.Length];
		int i = 0;
		foreach (LevelData.Collectable collectableData in LevelData.collectables) {
			Collectable collectable = GameObject.Instantiate<GameObject> (CollectablePrefab).GetComponent<Collectable> ();
			collectable.transform.position = new Vector3 (collectableData.x, collectableData.y, collectableData.z);
			Collected [i] = false;
			collectable.Init (Collect, i++);
			GeneratedObjects.Add (collectable.gameObject);
		}
	}

	private void UnloadLevel ()
	{
		if (Smartpad != null) {
			CalibratedSmartpad calibratedSmartpad = Smartpad.GetComponent<CalibratedSmartpad> ();
			Destroy (calibratedSmartpad.Offset.gameObject);
			Smartpad.Close ();
			Destroy (Smartpad.gameObject);
			Smartpad = null;
		}

		if (Hoverboard != null) {
			Destroy (Hoverboard.gameObject);
			Hoverboard = null;
		}

		foreach (GameObject generatedObject in GeneratedObjects) {
			Destroy (generatedObject);
		}
		GeneratedObjects.Clear ();
	}
}
