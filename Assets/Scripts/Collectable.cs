﻿using UnityEngine;
using System;

public class Collectable : MonoBehaviour
{
	private Func<Collectable, Void> Collect;

	public int I { get; private set; }

	public void Init (Func<Collectable, Void> collect, int i)
	{
		Collect = collect;
		I = i;
	}

	void OnTriggerEnter (Collider other)
	{
		Collect.Invoke (this);
	}
}
