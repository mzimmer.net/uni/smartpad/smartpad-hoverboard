﻿[System.Serializable]
public struct LevelData
{
	[System.Serializable]
	public struct Collectable
	{
		public float x;
		public float y;
		public float z;

		public override string ToString ()
		{
			return string.Format ("[Collectable: x={0}, y={1}, z={2}]", x, y, z);
		}
	}

	public string music;
	public float width;
	public float height;
	public float length;
	public float duration;
	public Collectable[] collectables;

	public override string ToString ()
	{
		switch (collectables.Length) {
		case 0:
			return string.Format ("[LevelData: music={0}, width={1}, height={2}, length={3}, duration={4}, collectables=]", music, width, height, length, duration);
		case 1:
			return string.Format ("[LevelData: music={0}, width={1}, height={2}, length={3}, duration={4}, collectables={5}]", music, width, height, length, duration, collectables [0]);
		default:
			return string.Format ("[LevelData: music={0}, width={1}, height={2}, length={3}, duration={4}, collectables={5}, ...]", music, width, height, length, duration, collectables [0]);
		}
	}
}
